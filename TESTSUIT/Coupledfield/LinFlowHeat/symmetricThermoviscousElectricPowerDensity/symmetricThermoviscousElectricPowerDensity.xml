<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Testcase for LinFlowHeatPDE in symmetric formulation to test the Electric-Power-Density</title>
    <authors>
      <author>Lorenz Klimon</author>
    </authors>
    <date>2022-12-09</date>
    <keywords>
      <keyword>CFD</keyword>
    </keywords>    
    <references>
    </references>
    <isVerified>no</isVerified>
    <description>
       Temperature, velocity and pressure field are compared with the solutions from the unsymmetric formulation.
       Note, that the unsymmetric formulation is not validated and therefore this testcase is also not validated.
       The aim of this testcase is, that the code for this coupling runs correctly in the symmetric formulation.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="symmetricThermoviscousElectricPowerDensity.h5ref"/>
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="S_fluid" material="FluidMat"/>
    </regionList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
       <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <sequenceStep index="1">
    <analysis>
      <transient>
            <numSteps>10</numSteps>
            <deltaT>1e-3</deltaT>
      </transient>
    </analysis>
    
    <pdeList>

      <elecConduction>
        <regionList>
          <region name="S_fluid" />
        </regionList>

        <bcsAndLoads>
          <ground name="L_bottom"/>
          <potential name="L_top" value ="100"/>
        </bcsAndLoads>

        <storeResults>
          <elemResult type="elecPowerDensity">
            <allRegions />
          </elemResult>
        </storeResults>
      </elecConduction>

      <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
        <regionList>
          <region name="S_fluid"/>
        </regionList>
        
        <presSurfaceList>
          <presSurf name="L_bottom"/>
          <presSurf name="L_top"/>
          <presSurf name="L_left"/>
          <presSurf name="L_right" />
        </presSurfaceList>
       
        <bcsAndLoads> 
          <pressure name="L_left" value="1"/>
          <noSlip name="L_top">       
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
          <noSlip name="L_bottom">       
            <comp dof="x"/>
            <comp dof="y" />
          </noSlip>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>      
        </storeResults>
      </fluidMechLin>
      
      <heatConduction>
        <regionList>
          <region name="S_fluid"/>
        </regionList>


        <bcsAndLoads>
          <heatTransport name="L_top" volumeRegion="S_fluid" heatTransferCoefficient="1" bulkTemperature="20.0"/>
          <heatTransport name="L_bottom" volumeRegion="S_fluid" heatTransferCoefficient="1" bulkTemperature="20.0"/>
          <elecPowerDensity name="S_fluid">
            <coupling pdeName="elecConduction">
              <quantity name="elecPowerDensity" />
            </coupling>
          </elecPowerDensity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
        </storeResults>       
      </heatConduction>
    </pdeList>
    
    <couplingList>
      <direct>
        <linFlowHeatDirect symmetric="yes">
          <regionList>
            <region  name="S_fluid"/>
          </regionList>
        </linFlowHeatDirect>
      </direct>
      <iterative>
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="yes">
           <quantity name="elecPower" value="1e-3" normType="rel"/>
        </convergence>
      </iterative>
    </couplingList>
  </sequenceStep>
</cfsSimulation>