<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  
  <documentation>
    <title>2D plate capacitor with hysteretic dielectric/piezoelectric material between the plates </title>
    <authors>
      <author>Michael Nierla</author>
    </authors>
    <date>2019-07-09</date>
    <keywords>
      <keyword>piezo</keyword>
      <keyword>electrostatic</keyword>
      <keyword>hysteresis</keyword>
      <keyword>linesearch</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>
      Geometry:
      Simple plate capacitor (size 3 m x3 m)
      Left and right hand edges are electrodes
      Top and bottom edges are default boundaries
      
      Sketch:
      
      y-axis
      |
      |___________________ air
      |                   |
      |                   |				
      |                   |
      |                   |
      |     dielectric/   |
      |     piezoelectric |
      |     material      |
      |                   |      
      |___________________|_______ x-axis
      
      Excitation:
      Left hand edge: Charge excitation
      Right hand edge: Ground
      
      Idea: 
      For given charge excitation Q on one plate and ground on the 
      other plate, the electric flux density between
      the capacitor plates is known to be
      D = Q
      
      - 1. D is independent of the permittivity of the dielectricum
      between the electrodes.
      - 2. E computes from D via E = D/eps(E)
      
      Aims of this test case:
      - Test if inverse problem can be solved with hysteresis model
      - Set Q > P_saturation
      - E has to peak such that D = eps0 + P can be fulfilled
      - let simulation run longer than the actual excitation signal in order to check if simulation 
         can handle a static input
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="nineElement.h5"/>
    </input> 
    <output>
      <hdf5/>
      <text id="txt1" fileCollect="entity"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    
    <regionList>
      <region name="probe" material="Pic255-ScalarPreisach"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="left"/>
      <surfRegion name="right"/>
      <surfRegion name="top"/>
      <surfRegion name="bot"/>
    </surfRegionList>
    
    <elemList>
      <elems name="center"></elems>
    </elemList>
  </domain>
  
  <sequenceStep>
    
    <analysis>
      <transient>
        <numSteps>175</numSteps>
        <deltaT>2</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <electrostatic>
        <regionList>
          <region name="probe" nonLinIds="h"/>
        </regionList>
        
        <nonLinList>
          <hysteresis id="h"/>
        </nonLinList>
        
        <bcsAndLoads>
          <charge name="left" value="0.6*sample1D('sig1.txt',t,1)"></charge>
          <constraint name="left"/>
          <ground name="right"/>
          <!-- <potential name="left"  value="15e6*sample1D('sig1.txt',t,1)"/>-->
          <!--        <fieldParallel name="top" volumeRegion="probe"/>
            <fieldParallel name="bot" volumeRegion="probe"/>-->
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions/>
          </nodeResult>
          
          <elemResult type="elecFieldIntensity">        
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>
          
          <elemResult type="elecPolarization">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>
          
          <elemResult type="elecFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt1"/>
            </elemList>
          </elemResult>
          
        </storeResults>
        
      </electrostatic>
      
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <hysteretic>
              <solutionMethod>
                <!-- start with fixpoint iteration, then switch to Newton scheme; update jacobian only every third iteration -->
                <QuasiNewton_FiniteDifferenceJacobian IterationsTillUpdate="3" initialNumberFPSteps="1"/>
              </solutionMethod>
              <lineSearch>
                <Backtracking_Armijo>
                  <maxIterLS>27</maxIterLS>
                  <etaStart>4.0E0</etaStart>
                  <etaMin>1.0E-4</etaMin>
                  <decreaseFactor>5.0E-1</decreaseFactor>
                  <rho>9.0E-1</rho>
                </Backtracking_Armijo>
                
              </lineSearch>

              <stoppingCriteria>
                <increment_relative value="1.25E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
                <residual_relative value="1.25E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
              </stoppingCriteria>
              <evaluationDepth>
                <evaluateAtIntegrationPoints_OneOperatorPerElement></evaluateAtIntegrationPoints_OneOperatorPerElement>
              </evaluationDepth>
              <failbackCriteria>
                <residual_relative value="2.5E-6" firstCheck="10" checkingFrequency="2" scaleToSystemSize="no"/>
                <residual_absolute value="2.5E-8" firstCheck="3" checkingFrequency="3" scaleToSystemSize="yes"/>
              </failbackCriteria>
              <maxIter>55</maxIter>
            </hysteretic>
            
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
    
  </sequenceStep>
</cfsSimulation>
