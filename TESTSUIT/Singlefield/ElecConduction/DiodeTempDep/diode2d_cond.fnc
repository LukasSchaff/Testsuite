-2.0000e+03 2.0000e+02 1.0000e3 # negative voltage break down
-2.0000e+03 5.0000e+02 1.0000e3

-1.0000e+03 2.0000e+02 3.3333e-11  # very good insulater up to -1000V
-1.0000e+03 5.0000e+02 9.9999e-03

-1.0000e+02 2.0000e+02 3.3333e-14 
-1.0000e+02 5.0000e+02 9.9999e-07

0.0000e+00 2.0000e+02 3.3333e-14
0.0000e+00 5.0000e+02 9.9999e-07

0.6000e+00 2.0000e+02 3.3333e-2 # at 0.6V the conductivity starts to increase dramatically
0.6000e+00 5.0000e+02 9.9999e-1

0.8000e00 2.0000e+02 3.0000e4
0.8000e00 5.0000e+02 9.0000e5

1. 2.0000e+02 6.5555e5
1. 5.0000e+02 3.8888e6 # at 1V the diode is full _on_ (saturation) compare to Al conductivity: 3e7
